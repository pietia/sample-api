package com.example.app

import org.scalatest.{FlatSpec, Matchers}

class InvitationSpec extends FlatSpec with Matchers {
  "#isValid" should "be false when data is missing" in {
    Invitation(null, null).isValid should be (false)
  }

  "#isValid" should "be false when data is invalid" in {
    Invitation("Johny", "asdas@").isValid should be (false)
    Invitation("Johny", "asdas wp.pl").isValid should be (false)
    Invitation("Johny", "").isValid should be (false)
    Invitation("", "asdad@wp.pl").isValid should be (false)
  }

  "#isValid" should "be true when data is valid" in {
    Invitation("Jan", "jan@wp.pl").isValid should be (true)
  }
}

