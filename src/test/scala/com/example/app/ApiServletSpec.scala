package com.example.app

import org.scalatra.test.specs2._
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods

import org.scalatra._
import org.scalatra.json._


trait JsonHelpers { self: ScalatraSpec =>
  def jsonBody: JValue = JsonMethods.parse(body)
  def serializeJson(jv: JValue): String = {
    JsonMethods.compact(JsonMethods.render(jv))
  }
}

// For more on Specs2, see http://etorreborre.github.com/specs2/guide/org.specs2.guide.QuickStart.html
class APIServletSpec extends ScalatraSpec with JsonHelpers {
  def is = s2"""
    GET /invitations on APIServlet
      should return 200  $getInvitations200
      should set properContentType $getInvitationsContentType
      should return hardcoded data $getInvitationsData
    POST /invitations on APIServlet
      should set properContentType $postInvitationsContentType
      should return 201 WHEN valid params passed $postInvitationsValidParams
      should return 422 WHEN only invitee passed $postInvitationsOnlyInvitee
      should return 422 WHEN valid email passed $postInvitationsOnlyEmail
      should return 422 WHEN invalid email passed $postInvitationsWrongEmail
      should return 422 WHEN invalid invitee passed $postInvitationsWrongInvitee
      should return 422 WHEN no params passed $postInvitationsNoParams
  """

  addServlet(classOf[ApiServlet], "/*")

  def getInvitations200 = get("/invitations") {
    status must_== 200
  }

  def getInvitationsContentType = get("/invitations") {
    header("Content-Type") must startWith ("application/json; charset=UTF-8")
  }

  def getInvitationsData = get("/invitations") {
    jsonBody must_== JsonMethods.render(List(("invitee" -> "John Smith") ~ ("email" -> "john@smith.mx")))
  }

  def postInvitationsContentType = post("/invitations") {
    header("Content-Type") must startWith ("application/json; charset=UTF-8")
  }

  def postInvitationsValidParams = post("/invitations", serializeJson(("invitee" -> "Jan") ~ ("email" -> "jan@kovalsky.com"))) {
    status must_== 201
  }

  def postInvitationsOnlyInvitee = post("/invitations", serializeJson("invitee" -> "Jan")) {
    status must_== 422
  }

  def postInvitationsOnlyEmail = post("/invitations", serializeJson("email" -> "jan@jan.com")) {
    status must_== 422
  }

  def postInvitationsWrongEmail = post("/invitations", serializeJson(("invitee" -> "Jan") ~ ("email" -> "jan"))) {
    status must_== 422
  }

  def postInvitationsWrongInvitee = post("/invitations", serializeJson(("invitee" -> "") ~ ("email" -> "jan"))) {
    status must_== 422
  }

  def postInvitationsNoParams = post("/invitations") {
    status must_== 422
  }

}
