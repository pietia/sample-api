package com.example.app

import org.scalatest.{FlatSpec, Matchers}

class FakeDaoSpec extends FlatSpec with Matchers {
  "#createInvitation" should "be true" in {
    new FakeDao().createInvitation(null) should be (true)
    new FakeDao().createInvitation(Invitation(null, null)) should be (true)
    new FakeDao().createInvitation(Invitation("Jan", "jan@jan.org")) should be (true)
  }

  "#getInvitations" should "should return fake data" in {
    new FakeDao().getInvitations should be (List(Invitation("John Smith", "john@smith.mx")))
  }

}
