package com.example.app

import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito._
import org.mockito.Matchers._


// consider using FunSpec or other letting us nest test cases
class RepositorySpec extends FlatSpec with Matchers with MockitoSugar {
  "#getInvitations" should "delegate call to Dao object/strategy" in {
    var mockedDao = mock[Dao]
    when(mockedDao.getInvitations()).thenReturn(List(Invitation("Aga", "aga@aga.aga")))

    val repository = new Repository(mockedDao)

    val invitations = repository.getInvitations


    invitations should be (List(Invitation("Aga", "aga@aga.aga")))
    verify(mockedDao).getInvitations()
  }

  "#craeteInvitation" should "delegate call to Dao object/strategy" in {
    var mockedDao = mock[Dao]

    val repository = new Repository(mockedDao)

    val invitation = Invitation("Ola", "ola@ola.ola")

    // case 1: created
    when(mockedDao.createInvitation(invitation)).thenReturn(true)

    repository.createInvitation(invitation) should be (true)
    verify(mockedDao).createInvitation(invitation)

    // reset mock and test the other case
    reset(mockedDao)

    // case 2: not created
    when(mockedDao.createInvitation(invitation)).thenReturn(false)

    repository.createInvitation(invitation) should be (false)

    verify(mockedDao).createInvitation(invitation)
  }

}

