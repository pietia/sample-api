package com.example.app

class FakeDao extends Dao {
  def getInvitations(): List[Invitation] = {
    List(Invitation("John Smith", "john@smith.mx"))
  }
  def createInvitation(invitation: Invitation) = true
}
