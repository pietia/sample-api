package com.example.app

case class Invitation(invitee: String, email: String) {

  private def isNonEmpty(string: String): Boolean = {
    Option(string).filter(_.trim.nonEmpty) match {
      case Some(s) => true
      case None => false
    }
  }

  private def isEmailValid(email: String): Boolean = {
    // yeah, i know ... didn't want to add any 3rd-party library.
    val EmailRegexp = """^\S+@\S+$""".r
    email match {
      case EmailRegexp() => true
      case _ => false
    }
  }

  def isValid: Boolean = {
    // or use fold a list
    isNonEmpty(invitee) && isNonEmpty(email) && isEmailValid(email)
  }
}