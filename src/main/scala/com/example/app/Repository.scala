package com.example.app

class Repository(dao: Dao) {
  def getInvitations: List[Invitation] = {
    dao.getInvitations()
  }
  def createInvitation(invitation: Invitation) = {
    Option(invitation) match {
      case Some(i) => if(invitation.isValid) dao.createInvitation(i) else false
      case _ => false
    }
  }
}

