package com.example.app

import org.scalatra._
import org.scalatra.json._
import org.json4s._

class ApiServlet extends SampleApiAppStack with JacksonJsonSupport {

  protected implicit val jsonFormats: Formats = DefaultFormats

  val repository = new Repository(new FakeDao())

  before() {
    contentType = formats("json")
  }

  get("/") {
    status = 404
  }

  get("/invitations") {
    repository.getInvitations
  }

  post("/invitations") {
    val invitation = parsedBody.extractOpt[Invitation].getOrElse(Invitation(null, null))
    //println("po sparsowaniu:", request.body, params, invitation)
    // println(invitation)
    repository.createInvitation(invitation) match {
      //case true => Created()
      case true => Created(invitation)
      case false => status = 422
    }
  }

}
