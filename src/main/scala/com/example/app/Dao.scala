package com.example.app

trait Dao {
  def getInvitations(): List[Invitation]
  def createInvitation(invitation: Invitation): Boolean
}