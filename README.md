# Sample API App

## Requirements

* sbt
* Scala

## How to run it?

* `./run.sh`
or
* `sbt`
* `container:start`

## Notes

- Decided to use `/invitations' instead of `/invitation` as it looks more REST'y
- FakeDao used to mimic persistence
- Used simplified regexp for email validation


